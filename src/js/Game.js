import mainImage from '../img/goblin.png';

export default class Game {
  constructor() {
    // eslint-disable-next-line prefer-destructuring
    this.startGame = [...document.getElementsByClassName('start')][0];
    this.score = document.getElementById('scoreItem');
    this.cells = [];
    this.n = 0;
    this.counter = 0;

    this.setField();
  }

  setField() {
    const field = document.createElement('div');
    field.classList.add('field');

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < 16; i++) {
      const cell = document.createElement('div');
      cell.classList.add('cell');
      this.cells.push(cell);

      field.append(cell);
    }

    document.body.append(field);

    this.setGoblin();

    document.body.addEventListener('click', (event) => {
      event.preventDefault();

      if (event.target.classList.contains('start')) {
        this.cells[Math.floor(Math.random() * 15)].append(this.goblin);

        this.runGoblin = setInterval(() => {
          this.cells.forEach((item) => {
            if (item.querySelector('[data-goblin]')) {
              field.insertBefore(item, this.cells[Math.floor(Math.random() * 15)]);
            }
          });
        }, 500);
      } else if (event.target.classList.contains('goblin')) {
        if (this.counter >= 2) {
          // eslint-disable-next-line no-alert
          alert('you win');
          event.target.remove();
          // eslint-disable-next-line no-multi-assign
          this.score.textContent = this.counter = 0;
          clearInterval(this.runGoblin);
          return;
        }
        // eslint-disable-next-line no-multi-assign
        this.score.textContent = this.counter += 1;
      }
    });
  }

  setGoblin() {
    this.goblin = document.createElement('img');
    this.goblin.setAttribute('src', mainImage);
    this.goblin.classList.add('goblin');
    this.goblin.dataset.goblin = 'goblin';
  }
}
